---
key: links
data:
  - label: 👔 LinkedIn
    url: https://www.linkedin.com/in/arthur-lutz-445a8a4a
  - label: 🐦 Twitter
    url: https://twitter.com/arthurlutz
  - label: 🦊 Gitlab
    url: https://gitlab.com/arthurlogilab
  - label: 🧑‍💻 Github 
    url: https://github.com/arthurlogilab
  - label: 🛠 Heptapod
    url: https://forge.extranet.logilab.fr/users/arthur/contributed
  - label: 🔧 Stackshare
    url: https://stackshare.io/arthurlogilab/my-stack
  - label: 🐘 Fediverse
    url: https://social.logilab.org/@arthurlutz
---
